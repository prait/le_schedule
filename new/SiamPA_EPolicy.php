<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header('Content-Type: application/json');
		
if (trim($_SERVER['PHP_AUTH_USER'])=="Fr@nk2Gi" and trim($_SERVER['PHP_AUTH_PW']) == "FwdGi2@20") {

	include_once("../include/misc.php");
	$conn = connect_db();

	$data = json_decode(file_get_contents('php://input'), true);

	$policy_id 	= $data["policy_id"];

	if ($policy_id != "") {
	
	$sql = "SELECT * FROM MAS_APPLICATION";
	$sql .= " WHERE POLICY_ID = '" . $policy_id ."'";
	$result = mysql_query($sql, $conn);
	if ($rs = mysql_fetch_array($result)) {
		// CREATE NEW PDF DOCUMENT
		include("setPDF.php");
		//----------------------------------------------------------------------------------------------------------------------------------------------------------------------//			
		$pdf->AddPage();
		//$pdf->SetProtection($permissions=array('print', 'copy'), $user_pass=$password, $owner_pass=null, $mode=0, $pubkeys=null);
		$pdf->Image('images/FWD_logo_full_colour_CMYK.png', 6, 8, 30, 20);            
	  	//$pdf->Text(70,20,'=== ตัวอย่างเพื่อทดสอบเท่านั้น ===');
		$pdf->SetFont('cordiaupc', '', 10);
		$pdf->SetXY(10,22);
		$pdf->Cell(208,8,'44/1 อาคารรุ่งโรจน์ธนกุล ชั้น 12 ถนนรัชดาภิเษก แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพฯ 10310 โทรศัพท์ 0-2202-9500 โทรสาร 0-2202-9555',0,0,'L'); 
		$pdf->SetXY(10,25);
		$pdf->Cell(208,8,'44/1 12 Fl. Rungrojthanakul Bldg., Ratchadapisek Rd., Huai Khwang, Bangkok 10310 Tel. 0-2202-9500 Fax: 0-2202-9555 ',0,0,'L');
		$pdf->Text(159,27,'              ทะเบียนเลขที่   0107555000597');
		$pdf->Text(159,30,'เลขประจำตัวผู้เสียภาษี   0107555000597');
		$pdf->SetFont('angsanaupc', ' B', 16);
		$pdf->MultiCell(97, 10, "SCI", 0, 'L', 0, 0, 30, 41, true);
		$pdf->SetFont('angsanaupc', ' B', 14);
		$pdf->MultiCell(190, 8, "", 1, 'C', 0, 0, 10, 31.7, true); 
		$pdf->MultiCell(190, 8, "ตารางกรมธรรม์ประกันภัย  THE SCHEDULE", 0, 'C', 0, 0, 10, 31.7, true); 
		$pdf->MultiCell(190, 8, "ต้นฉบับ/ORIGINAL", 0, 'R', 0, 0, 10, 31.7, true); 
		$pdf->SetFont('angsanaupc', ' B', 12);
		$pdf->MultiCell(190,12, "การประกันภัยอุบัติเหตุส่วนบุคคลสยามซิตี้", 1, 'C', 0, 0, 10, 40, true); 
		$pdf->SetFont('angsanaupc', ' N', 12);
		$pdf->MultiCell(190, 8, "รหัสบริษัท", 0, 'L', 0, 0, 10, 40, true);
		$pdf->MultiCell(190, 8, "Company Code", 0, 'L', 0, 0, 10, 44, true);
		$pdf->MultiCell(50, 8, "กรมธรรม์ประกันภัยเลขที่", 0, 'L', 0, 0, 140, 40, true);
		$pdf->MultiCell(50, 8, "Policy No.", 0, 'L', 0, 0, 140, 44, true);
		$pdf->MultiCell(190, 8, "คุ้มครอง 24 ชั่วโมงทั่วโลก 24 Hours Worldwide Coverage", 0, 'C', 0, 0, 10, 45, true);

		$pdf->MultiCell(130, 24, "1.  ผู้เอาประกันภัย : ชื่อและที่อยู่ The Insured : Name and Address", 1, 'L', 0, 0, 10, 52, true); 
		$pdf->MultiCell(60, 24, "เลขประจำตัวประชาชน : ID No.", 1, 'L', 0, 0, 140, 52, true); 
		$pdf->MultiCell(60, 8, "อาชีพ : Occupation", 0, 'L', 0, 0, 140, 57, true); 
		$pdf->MultiCell(60, 8, "ชั้นอาชีพ : Occupation class", 0, 'L', 0, 0, 140, 62, true); 
		$pdf->MultiCell(60, 8, "อายุ : Age", 0, 'L', 0, 0, 140, 67, true); 
		$pdf->MultiCell(130, 24, "2.  ผู้รับประโยชน์ : ชื่อและที่อยู่ The Beneficiary : Name and Address", 1, 'L', 0, 0, 10, 76, true); 
		$pdf->MultiCell(60, 24, "ความสัมพันธ์กับผู้เอาประกันภัย", 1, 'L', 0, 0, 140, 76, true); 
		$pdf->MultiCell(60, 12, "Relationship to the Insured", 0, 'L', 0, 0, 140, 81, true); 
		$pdf->MultiCell(190, 12, "3.  ระยะเวลาประกันภัย :       เริ่มต้นวันที่                                                            เวลา                น.        สิ้นสุดวันที่                                                            เวลา  16.30 น.", 1, 'L', 0, 0, 10, 100, true); 
		$pdf->MultiCell(190, 12, "Period of Insurance :         Form                                                                      at                    hours   To                                                                                at hours", 0, 'L', 0, 0, 13, 104, true); 
		$pdf->MultiCell(190, 12, "4.  จำนวนจำกัดความรับผิด : กรมธรรม์ประกันภัยนี้ให้การคุ้มครองเฉพาะผลของการบาดเจ็บทางร่างกายในข้อที่มีจำนวนเงินผลประโยชน์ระบุไว้เท่านั้น", 1, 'L', 0, 0, 10, 112, true); 
		$pdf->MultiCell(190, 12, "Limit of Liability : This policy affords coverage only with respect to such result from bodily injury for which a sum insured is stated.", 0, 'L', 0, 0, 13, 117, true); 

		$POLICY_BDATE	= substr($rs["tempLifeBirth"], -4)."-".substr($rs["tempLifeBirth"],3,2)."-".substr($rs["tempLifeBirth"],0,2);
		$age = date("Y", strtotime($rs["POLICY_TRANDATE"])) - date("Y", strtotime($POLICY_BDATE));

		$pdf->MultiCell(60, 24, $rs['POLICY_ID'], 0, 'R', 0, 0, 140, 40, true); 
		$pdf->MultiCell(130, 24, $rs['tempTitleLifeNm'].$rs['tempFirstLifeNm']." ".$rs['tempLastLifeNm'], 0, 'L', 0, 0, 13, 57, true); 
		$pdf->MultiCell(130, 24, "เลขที่ ".$rs['tempLifeAddrNo']." ".$rs['tempLifeBuilding'], 0, 'L', 0, 0, 13, 62, true); 
		$pdf->MultiCell(130, 24, $rs['tempLifeTambol']." ".$rs['tempLifeAmphur']." ".$rs['tempLifeChangewat']." รหัสไปรษณีย์ ".$rs['tempLifePostcode'], 0, 'L', 0, 0, 13, 67, true); 
		$pdf->MultiCell(60, 24, $rs['tempLifeIc'], 0, 'R', 0, 0, 140, 52, true); 
		$pdf->MultiCell(60, 24, $age."   ปี", 0, 'L', 0, 0, 155, 67, true); 
		$pdf->MultiCell(130, 24, $rs['tempTitleBeneNm'].$rs['tempFirstBeneNm']." ".$rs['tempLastBeneNm'], 0, 'L', 0, 0, 13, 81, true); 
		$pdf->MultiCell(60, 24, $rs['tempRelation'], 0, 'R', 0, 0, 140, 81, true); 
		$pdf->Text(60,105, PrintDate($rs['POLICY_COMDATE']));
		if($rs['POLICY_COMDATE'] > $rs['POLICY_TRANDATE']) {
			$pdf->Text(105,105,"00.01");
		} else {
			$pdf->Text(105,105,substr($rs['POLICY_TRANDATE'],11,5));
		}
		//$pdf->Text(105,105,substr($rs['POLICY_TRANDATE'],11,5));
		$pdf->Text(140,105, PrintDate($rs['POLICY_EXPDATE']));

		$pdf->MultiCell(75, 12, "ข้อตกลงคุ้มครอง / เอกสารแนบท้าย", 1, 'C', 0, 0, 10, 124, true);
		$pdf->MultiCell(75, 12, "Insuring Agreement / Endorsement", 0, 'C', 0, 0, 10, 129, true);
		$pdf->MultiCell(40, 12, "จำนวนเงินเอาประกันภัย (บาท)", 1, 'C', 0, 0, 85, 124, true);
		$pdf->MultiCell(40, 12, "Sum Insured (Baht)", 0, 'C', 0, 0, 85, 129, true);
		$pdf->MultiCell(45, 12, "ความรับผิดส่วนแรก (บาท หรือ วัน)", 1, 'C', 0, 0, 125, 124, true);
		$pdf->MultiCell(45, 12, "Deductble (Baht or days)", 0, 'C', 0, 0, 125, 129, true);
		$pdf->MultiCell(30, 12, "เบี้ยประกันภัย (บาท)", 1, 'C', 0, 0, 170, 124, true);
		$pdf->MultiCell(30, 12, "Premium (Baht)", 0, 'C', 0, 0, 170, 129, true);

		$sql_cover = "SELECT A.*, B.COVERAGE_NAME FROM scil.TXN_PACKAGE_COVERAGE A LEFT JOIN scil.LTB_COVERAGE B";
		$sql_cover .= " ON A.CID = B.CID";
		$sql_cover .= " WHERE A.PKG_ID = '".$rs['tempFormula']."' ";
		$sql_cover .= " AND B.COVER_TYPE = '1' ";
//		$sql_cover .= " AND (B.COVER_TYPE = '1' or B.COVER_TYPE = '2')";
		$sql_cover .= " ORDER BY A.CID";
		//echo $sql;
		$query_cover = mysql_query($sql_cover) or die (mysql_error());
		$numrows_cover = mysql_num_rows($query_cover);

		$sety = 136;					
		$pdf->SetFont('angsanaupc', 'N', 10);
		$pdf->SetXY(10,$sety);
							
		for($j=1; $j<=$numrows_cover; $j++) { // loop
		$row_cover = mysql_fetch_array($query_cover);

		$package_id = $row_cover["PKG_ID"];
		$cover =  $row_cover["COVERAGE_NAME"];
		$sum_insure =  $row_cover["COVERAGE"];
		$premium =  $row_cover["COVERAGE_PREM"];

		if ($package_id == 'UNS01'){	
			$sum_insure1_1 = '100000';
			$sum_insure1_2 = '50000';
			$sum_insure1_3 = '50000';
			$sum_insure2 = '100000';
			$sum_insure3 = '10000';
			$sum_insure3_1 = '5000';
			$sum_insure4 = '60000';
			$sum_insure5 = '20000';
			$sum_insure6 = '10000';
			$sum_insure7 = '10000';		
		}else if($package_id == 'UNS02'){
			$sum_insure1_1 = '150000';
			$sum_insure1_2 = '75000';
			$sum_insure1_3 = '75000';
			$sum_insure2 = '150000';
			$sum_insure3 = '15000';
			$sum_insure3_1 = '7500';
			$sum_insure4 = '90000';
			$sum_insure5 = '20000';
			$sum_insure6 = '10000';
			$sum_insure7 = '15000';
		}else if($package_id == 'UNS03'){
			$sum_insure1_1 = '200000';
			$sum_insure1_2 = '100000';
			$sum_insure1_3 = '100000';
			$sum_insure2 = '200000';
			$sum_insure3 = '20000';
			$sum_insure3_1 = '10000';
			$sum_insure4 = '120000';
			$sum_insure5 = '20000';
			$sum_insure6 = '10000';
			$sum_insure7 = '20000';
		}else if($package_id == 'UNS04'){
			$sum_insure1_1 = '500000';
			$sum_insure1_2 = '250000';
			$sum_insure1_3 = '250000';
			$sum_insure2 = '500000';
			$sum_insure3 = '30000';
			$sum_insure3_1 = '15000';
			$sum_insure4 = '240000';
			$sum_insure5 = '20000';
			$sum_insure6 = '10000';
			$sum_insure7 = '30000';
		}else if($package_id == 'UNS05'){
			$sum_insure1_1 = '800000';
			$sum_insure1_2 = '400000';
			$sum_insure1_3 = '400000';
			$sum_insure2 = '800000';
			$sum_insure3 = '40000';
			$sum_insure3_1 = '20000';
			$sum_insure4 = '360000';
			$sum_insure5 = '20000';
			$sum_insure6 = '10000';
			$sum_insure7 = '40000';
		}else if($package_id == 'UNS06'){
			$sum_insure1_1 = '1000000';
			$sum_insure1_2 = '500000';
			$sum_insure1_3 = '500000';
			$sum_insure2 = '1000000';
			$sum_insure3 = '50000';
			$sum_insure3_1 = '25000';
			$sum_insure4 = '480000';
			$sum_insure5 = '20000';
			$sum_insure6 = '10000';
			$sum_insure7 = '50000';
		}

		$agr_12mths = ($sum_insure4/12);

/*-----  Print Sum Insure On Card -------*/		
		if($package_id=='UNS01') {
			$sum_card1 = '10000' ;
			$sum_card2 = '5000' ;
			$pck_name = 'Unseen PA Plan 1';
		} else if($package_id=='UNS02') {
			$sum_card1 = '15000' ;
			$sum_card2 = '7500' ;
			$pck_name = 'Unseen PA Plan 2';
		} else if($package_id=='UNS03') {
			$sum_card1 = '20000' ;
			$sum_card2 = '10000' ;
			$pck_name = 'Unseen PA Plan 3';
		} else if($package_id=='UNS04') {
			$sum_card1 = '30000' ;
			$sum_card2 = '15000' ;
			$pck_name = 'Unseen PA Plan 4';
		} else if($package_id=='UNS05') {
			$sum_card1 = '40000' ;
			$sum_card2 = '20000' ;
			$pck_name = 'Unseen PA Plan 5';
		} else if($package_id=='UNS06') {
			$sum_card1 = '50000' ;
			$sum_card2 = '25000' ;
			$pck_name = 'Unseen PA Plan 6';
		}

			if(substr($package_id,0,3) == "PAO") {
				$pdf->MultiCell(75, 8," - ".$cover, 0, 'L', 0, 0, 10, $sety, true);
				$pdf->MultiCell(40, 8,number_format($sum_insure,0), 0, 'R', 0, 0, 85, $sety, true);
				$pdf->MultiCell(75, 8,number_format($premium,2), 0, 'R', 0, 0, 125, $sety, true);
				$pdf->Ln();
				$sety = $sety+10;
				$pdf->SetXY(10,$sety);
			} else if(substr($package_id,0,3) == "UNS") {
				$pdf->MultiCell(75, 8," - ".$cover, 0, 'L', 0, 0, 10, $sety, true);
				$pdf->MultiCell(40, 8,number_format($sum_insure,0), 0, 'R', 0, 0, 85, $sety, true);
				$pdf->MultiCell(75, 8,number_format($premium,2), 0, 'R', 0, 0, 125, $sety, true);
				$pdf->Ln();
				$sety = $sety+6;
				$pdf->SetXY(10,$sety);
			} else {
				$pdf->MultiCell(75, 8," - ".$cover, 0, 'L', 0, 0, 10, $sety, true);
				$pdf->MultiCell(40, 8,number_format($sum_insure,0), 0, 'R', 0, 0, 85, $sety, true);
				$pdf->MultiCell(75, 8,number_format($premium,2), 0, 'R', 0, 0, 125, $sety, true);
				$pdf->Ln();
				$sety = $sety+6;
				$pdf->SetXY(10,$sety);
			}
							
		}

		$pdf->MultiCell(75, 73, "", 1, 'C', 0, 0, 10, 136, true);
		$pdf->MultiCell(40, 73, "", 1, 'C', 0, 0, 85, 136, true);
		$pdf->MultiCell(45, 73, "", 1, 'C', 0, 0, 125, 136, true);
		$pdf->MultiCell(30, 73, "", 1, 'C', 0, 0, 170, 136, true);

		$pdf->MultiCell(160, 32, "", 1, 'C', 0, 0, 10, 209, true);
		$pdf->MultiCell(30, 32, "", 1, 'C', 0, 0, 170, 209, true);

		$sql_package = "SELECT * FROM scil.MAS_PACKAGE2";
		$sql_package .= " WHERE PKG_ID = '".$rs['tempFormula']."' ";
		$result_package = mysql_query($sql_package, $conn);
		if ($rs_package = mysql_fetch_array($result_package)) {
			$pkg_name = $rs_package["PKG_NAME"];
			$price = $rs_package["NET_PREM"];
			$stamp = $rs_package["STAMP"];
			$prem_add = $rs_package["ADD_PREM"];
			$prem_disc = $rs_package["DISC_PREM"];
			$total_premium = $price+$stamp;
			$care_card		= $rs_package["CARE_CARD"];

		}

		$pdf->SetFont('angsanaupc', 'N', 12);
		$pdf->MultiCell(115, 8, "เบี้ยประกันภัยสำหรับภัยเพิ่ม / Addittional Premium", 0, 'L', 0, 0, 110, 209, true);
		$pdf->MultiCell(115, 8, "ส่วนลดเบี้ยประกันภัย / Premium Discount", 0, 'L', 0, 0, 110, 214, true);
		$pdf->MultiCell(115, 8, "เบี้ยประกันภัยสุทธิ / Net Premium", 0, 'L', 0, 0, 110, 219, true);
		$pdf->MultiCell(115, 8, "อากร / Stamps", 0, 'L', 0, 0, 110, 224, true);
		$pdf->MultiCell(115, 8, "ภาษี / Tax", 0, 'L', 0, 0, 110, 229, true);
		$pdf->MultiCell(115, 8, "เบี้ยประกันภัยรวม / Total Premium", 0, 'L', 0, 0, 110, 234, true);

		$pdf->MultiCell(30, 32, number_format($prem_add,2), 0, 'R', 0, 0, 170, 209, true);
		$pdf->MultiCell(30, 32, number_format($prem_disc,2), 0, 'R', 0, 0, 170, 214, true);
		$pdf->MultiCell(30, 32, number_format($price,2), 0, 'R', 0, 0, 170, 219, true);
		$pdf->MultiCell(30, 32, number_format($stamp,2), 0, 'R', 0, 0, 170, 224, true);
		$pdf->MultiCell(30, 32, "0.00", 0, 'R', 0, 0, 170, 229, true);
		$pdf->MultiCell(30, 32, number_format($total_premium,2), 0, 'R', 0, 0, 170, 234, true);

		$pdf->MultiCell(190, 12, "", 1, 'C', 0, 0, 10, 241, true);
		$pdf->Image('images/checkbox-square.png', 11, 244, 5, 5); 
		$pdf->MultiCell(30, 10, "ประกันภัยตรง", 0, 'L', 0, 0, 17, 242, true);
		$pdf->MultiCell(30, 10, "Direct", 0, 'L', 0, 0, 17, 246, true);
		$pdf->Image('images/checkbox-square.png', 37, 244, 5, 5); 
		$pdf->MultiCell(30, 10, "ตัวแทนประกันวินาศภัย", 0, 'L', 0, 0, 43, 242, true);
		$pdf->MultiCell(30, 10, "Agent", 0, 'L', 0, 0, 43, 246, true);
		$pdf->Text(74,247, "X");
		$pdf->Image('images/checkbox-square.png', 73, 244, 5, 5); 
		$pdf->MultiCell(60, 10, "นายหน้าประกันวินาศภัย", 0, 'L', 0, 0, 79, 242, true);
		$pdf->MultiCell(60, 10, "Broker", 0, 'L', 0, 0, 79, 246, true);
		$pdf->MultiCell(70, 10, "ใบอนุญาตเลขที่", 0, 'L', 0, 0, 160, 242, true);
		$pdf->MultiCell(70, 10, "License No.", 0, 'L', 0, 0, 160, 246, true);

		$pdf->Text(90,251, "บริษัท โบลท์เทค อินชัวรันส์ โบรคเกอร์ (ประเทศไทย) จำกัด");
		$pdf->Text(182,251, "ว00017/2559");
		$pdf->Text(55,257, PrintDate($rs['POLICY_TRANDATE']));
		$pdf->Text(160,257, PrintDate($rs['POLICY_TRANDATE']));

		$pdf->MultiCell(60, 10, "วันทำสัญญาประกันภัย", 0, 'L', 0, 0, 17, 252, true);
		$pdf->MultiCell(60, 10, "Agreement made on", 0, 'L', 0, 0, 17, 255, true);
		$pdf->MultiCell(60, 10, "วันออกกรมธรรม์ประกันภัย", 0, 'L', 0, 0, 120, 252, true);
		$pdf->MultiCell(60, 10, "Policy issued on", 0, 'L', 0, 0, 120, 255, true);

		$pdf->MultiCell(190, 10, "เพื่อเป็นหลักฐาน บริษัท โดยผู้มีอำนาจกระทำการแทนบริษัท ได้ลงลายมือชื่อและประทับตราของบริษัทไว้เป็นสำคัญ ณ สำนักงานของบริษัท", 0, 'L', 0, 0, 17, 261, true); 
		$pdf->MultiCell(190, 10, "As evidence, the Company has caused this policy to be signed by duly authorized persons and the Company's stamp to be affixed at Its office.", 0, 'L', 0, 0, 17, 265, true); 

		// -- ลายเซ็นต์ ---//
		$pdf->Image('images/kulawat.jpg', 25, 274, 18, 9);
		$pdf->Image('images/BOB.jpg', 90, 273, 20, 15);
		$pdf->Image('images/BOB.jpg', 160, 273, 20, 15);

		$pdf->Image('images/SEAL_FWDGI.png', 62, 272, 17, 17);
		
		$pdf->Text(11,283,'___________________________________');
		$pdf->Text(24,287,'กรรมการ - Director');
		$pdf->Text(80,283,'____________________________________');
		$pdf->Text(92,287,'กรรมการ - Director');
		$pdf->Text(150,283,'___________________________________');
		$pdf->Text(153,287,'ผู้รับมอบอำนาจ - Authorized Signature');
		//$pdf->Text(153,291,'....................../................../..................');

/*===========  PRINT ATTACHMENT =================*/
	if(substr($package_id,0,3) == "UNS") {
			$pdf->AddPage();

			$pdf->Image('images/FWD_logo_full_colour_CMYK.png', 5, 4, 30, 20);   
			$pdf->Image('images/SEAL_FWDGI.png', 175, 255, 17, 17);
			//$pdf->Text(70,17,'=== ตัวอย่างเพื่อทดสอบเท่านั้น ===');

			$pdf->SetFont('angsanaupc', '', 10);
			$pdf->MultiCell(193, 10, "44/1 อ.รุ่งโรจน์ธนกุล ชั้น 12 ถ.รัชดาภิเษก  แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310 Tel: +66 (0) 2202 9500 Fax: +66 (0) 2202 9555", 0, 'L', 0, 0, 9, 18, true); 
			$pdf->MultiCell(193, 10, "ทะเบียนเลขที่  0107555000597", 0, 'R', 0, 0, 7, 18, true); 
			$pdf->MultiCell(193, 10, "44/1 12 Flr, Rungrojthanakul Bldg., Ratchadaphisek Rd., Huay Khwang,  Bangkok 10310 Tel: +66 (0) 2202 9500 Fax: +66 (0) 2202 9555", 0, 'L', 0, 0, 9, 21, true); 
			$pdf->MultiCell(193, 10, "เลขประจำตัวผู้เสียภาษี  0107555000597", 0, 'R', 0, 0, 7, 21, true); 

			$pdf->SetFont('angsanaupc', '', 12);
			$pdf->MultiCell(190, 10, "รหัสบริษัท                     เอกสารแนบท้ายเป็นส่วนหนึ่งของกรมธรรม์ประกันภัยเลขที่", 1, 'L', 0, 0, 10, 27, true);
			$pdf->MultiCell(190, 10, "วันทำเอกสาร", 0, 'L', 0, 0, 140, 27, true);
			$pdf->SetFont('angsanaupc', '', 10);
			$pdf->MultiCell(190, 10, "Company Code                    Attaching to and forming part of Policy No.", 0, 'L', 0, 0, 10, 31, true);
			$pdf->MultiCell(190, 10, " Date of Issue", 0, 'L', 0, 0, 140, 31, true);
					$pdf->SetFont('angsanaupc', '', 12);
			$pdf->MultiCell(190,10, "ชื่อผู้เอาประกันภัย", 1, 'L', 0, 0, 10, 37, true); 
			$pdf->SetFont('angsanaupc', '', 10);
			$pdf->MultiCell(190,10, "Name of Insured", 0, 'L', 0, 0, 10, 41, true); 

			$pdf->SetFont('angsanaupc', '', 12); 
			$pdf->MultiCell(190,10, "ระยะเวลามีผลบังคับ  :  เริ่มวันที่                                                                   เวลา                   น.", 1, 'L', 0, 0, 10, 47, true);
			$pdf->MultiCell(190,10, "สิ้นสุดวันที่                                                            เวลา                   น.", 0, 'L', 0, 0, 120, 47, true);
			$pdf->SetFont('angsanaupc', '', 10);
			$pdf->MultiCell(190,10, "Period of Insurance  :  Effective Date", 0, 'L', 0, 0, 10, 51, true);
			$pdf->MultiCell(190,10, " at                        Hours.", 0, 'L', 0, 0, 92, 51, true);
			$pdf->MultiCell(190,10, "Expiry Date", 0, 'L', 0, 0, 120, 51, true);
			$pdf->MultiCell(190,10, "at                        Hours.", 0, 'L', 0, 0, 175, 51, true);

			$pdf->SetFont('angsanaupc', 'B', 12);
			$pdf->MultiCell(190, 10, "SCI", 0, 'L', 0, 0, 28, 27, true);

			$pdf->MultiCell(190, 10, $rs['POLICY_ID'], 0, 'L', 0, 0, 110, 27, true);
			$pdf->MultiCell(190, 10, PrintDate($rs['POLICY_TRANDATE']), 0, 'L', 0, 0, 160, 27, true);
			$pdf->MultiCell(130, 10, $rs['tempTitleLifeNm'].$rs['tempFirstLifeNm']." ".$rs['tempLastLifeNm'], 0, 'L', 0, 0, 45, 37, true); 
			$pdf->Text(55,53, PrintDate($rs['POLICY_COMDATE'])); 
			$pdf->MultiCell(190, 12, substr($rs['POLICY_TRANDATE'],11,5), 0, 'L', 0, 0, 101, 48, true);
			$pdf->Text(140,53, PrintDate($rs['POLICY_EXPDATE'])); 
			$pdf->Text(183,53, "16:30"); 

			$pdf->MultiCell(190, 220, "", 1, 'C', 0, 0, 10, 58, true); 

			$pdf->SetFont('angsanaupc', '', 10); 
			$pdf->MultiCell(190, 4, "เป็นที่ตกลงและเข้าใจกันว่า ถ้าข้อความใดในเอกสารแนบนี้ขัดหรือแย้งกับข้อความที่ปรากฏในกรมธรรม์ประกันภัยนี้ ให้ถือข้อความตามที่ปรากฏในเอกสารนี้บังคับแทน", 0, 'L', 0, 0, 10, 59, true); 
			$pdf->MultiCell(190, 4, "Notwithstanding anything contained in the policy to the contrary, it is hereby note and agreed that the following specification prevails :-", 0, 'L', 0, 0, 10, 61, true); 

			$pdf->SetFont('angsanaupc', 'B', 12); 
			$pdf->MultiCell(190, 4, "แผนความคุ้มครอง ".$pkg_name, 0, 'C', 0, 0, 10, 68, true);

			$pdf->SetFont('angsanaupc', '', 12); 
			$pdf->MultiCell(190, 4, "1. ผลประโยชน์การเสียชีวิต การสูญเสียอวัยวะ สายตา หรือทุพพลภาพถาวรสิ้นเชิง (อ.บ.1)", 0, 'L', 0, 0, 10, 78, true);
			$pdf->MultiCell(190, 4, "1.1 จากอุบัติเหตุทั่วไป", 0, 'L', 0, 0, 25, 82, true);
			$pdf->MultiCell(190, 4, "1.2 จากการถูกฆาตกรรม หรือถูกทำร้ายร่างกาย", 0, 'L', 0, 0, 25, 87, true);
			$pdf->MultiCell(190, 4, "1.3 จากการขับขี่ หรือ โดยสารรถจักรยานยนต์", 0, 'L', 0, 0, 25, 92, true);
			$pdf->MultiCell(190, 4, "2. ผลประโยชน์การเสียชีวิต การสูญเสียอวัยวะ สายตา หรือทุพพลภาพถาวรสิ้นเชิง เนื่องจากอุบัติเหตุสาธารณะ", 0, 'L', 0, 0, 10, 97, true);
			$pdf->MultiCell(190, 4, "   (จ่ายเพิ่มอีกจากข้อ 1) ไม่รวมจากการถูกฆาตกรรมหรือถูกทำร้ายร่างกาย หรือจากการขับขี่หรือโดยสารรถจักรยานยนต์", 0, 'L', 0, 0, 10, 102, true);
			$pdf->MultiCell(190, 4, "3. ค่ารักษาพยาบาลเนื่องจากอุบัติเหตุ (ไม่รวมสาเหตุที่มาจากการขับขี่หรือโดยสารรถจักรยานยนต์)", 0, 'L', 0, 0, 10, 107, true);
			$pdf->MultiCell(190, 4, "3.1 ขยายความคุ้มครองค่ารักษาพยาบาลจากการขับขี่ หรือโดยสารรถจักรยานยนต์", 0, 'L', 0, 0, 25, 112, true);
			$pdf->MultiCell(190, 4, "4. เงินช่วยเหลือครอบครัว กรณีผู้เอาประกันภัยเสียชีวิตจากอุบัติเหตุ เดือนละ ".number_format($agr_12mths, 2)." บาท ต่อเนื่อง 12 เดือน", 0, 'L', 0, 0, 10, 117, true);
			$pdf->MultiCell(190, 4, "5. เงินชดเชยหนี้ค้างชำระตามกฏหมาย", 0, 'L', 0, 0, 10, 122, true);
			$pdf->MultiCell(190, 4, "6. เงินชดเชยภาษีเงินได้บุคคลธรรมดา", 0, 'L', 0, 0, 10, 127, true);
			$pdf->MultiCell(190, 4, "7. ค่าปลงศพ หรือค่าใช้จ่ายในการจัดการงานศพ กรณีการเสียชีวิตจากการเจ็บป่วย (ระยะเวลารอคอย180 วัน)", 0, 'L', 0, 0, 10, 132, true);

			$pdf->MultiCell(190, 4, number_format($sum_insure1_1, 0)." บาท", 0, 'R', 0, 0, 5, 82, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure1_2, 0)." บาท", 0, 'R', 0, 0, 5, 87, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure1_3, 0)." บาท", 0, 'R', 0, 0, 5, 92, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure2, 0)." บาท", 0, 'R', 0, 0, 5, 97, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure3, 0)." บาท", 0, 'R', 0, 0, 5, 107, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure3_1, 0)." บาท", 0, 'R', 0, 0, 5, 112, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure4, 0)." บาท", 0, 'R', 0, 0, 5, 117, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure5, 0)." บาท", 0, 'R', 0, 0, 5, 122, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure6, 0)." บาท", 0, 'R', 0, 0, 5, 127, true);
			$pdf->MultiCell(190, 4, number_format($sum_insure7, 0)." บาท", 0, 'R', 0, 0, 5, 132, true);

			$pdf->MultiCell(190, 4, "เบี้ยประกันภัยรวมอากรแสตมป์และภาษี เป็นจำนวนเงิน ".number_format($total_premium, 0)." บาท", 0, 'L', 0, 0, 30, 142, true);

/*===========  PRINT CARE CARD =================*/

			$pdf->AddPage();

			$pdf->Image('images/Care_Card_TPA.jpg', 45, 30, 114, 163);    
		
			//$pdf->Text(70,17,'=== ตัวอย่างเพื่อทดสอบเท่านั้น ===');

			if ($care_card == "TYPE1"){  /*---  For UNSEEN  && LADY --*/
					$pdf->SetFont('cordiaupc', '', 10);
					$pdf->MultiCell(190, 10, "กรมธรรม์เลขที่", 0, 'L', 0, 0, 65, 71, true);
					$pdf->MultiCell(190, 10, "บัตรประจำตัวประชาชนเลขที่", 0, 'L', 0, 0, 65, 75, true);
					$pdf->MultiCell(190, 10, "ชื่อผู้เอาประกัน :", 0, 'L', 0, 0, 65, 79, true);

					$pdf->MultiCell(190, 10, "ค่ารักษาพยาบาลจากอุบัติเหตุ", 0, 'L', 0, 0, 65, 83, true);
					$pdf->MultiCell(190, 10, "- กรณี อุบัติเหตุทั่วไป", 0, 'L', 0, 0, 70, 87, true);
					$pdf->MultiCell(190, 10, "- กรณี รถจักรยานยนต์", 0, 'L', 0, 0, 70, 91, true);

					$pdf->MultiCell(190, 10, "วันที่มีผลบังคับ", 0, 'L', 0, 0, 65, 95, true);
					$pdf->MultiCell(190, 10, "วันที่สิ้นสุด", 0, 'L', 0, 0, 105, 95, true);
					$pdf->MultiCell(190, 10, "ชื่อแผนประกันภัย", 0, 'L', 0, 0, 65, 99, true);

					$pdf->SetFont('cordiaupc', 'B', 10);
					$pdf->MultiCell(190, 10, $rs['POLICY_ID'], 0, 'L', 0, 0, 97, 71, true); 
					$pdf->MultiCell(190, 10, $rs['tempLifeIc'], 0, 'L', 0, 0, 97, 75, true); 
					$pdf->MultiCell(190, 10, $rs['tempTitleLifeNm'].$rs['tempFirstLifeNm']." ".$rs['tempLastLifeNm'], 0, 'L', 0, 0, 97, 79, true); 
					$pdf->MultiCell(50, 10, number_format($sum_card1,0)." บาท", 0, 'R', 0, 0, 62, 87, true);
					$pdf->MultiCell(50, 10, number_format($sum_card2,0)." บาท", 0, 'R', 0, 0, 62, 91, true);
					$pdf->MultiCell(190, 10, PrintDate($rs['POLICY_COMDATE']), 0, 'L', 0, 0, 80, 95, true);
					$pdf->MultiCell(190, 10, PrintDate($rs['POLICY_EXPDATE']), 0, 'L', 0, 0, 116, 95, true);
					$pdf->MultiCell(190, 10, $pck_name, 0, 'L', 0, 0, 97, 99, true);
		
			}

			$pdf->AddPage();
			$pdf->Image('images/TPA_Hospital_1.jpg', 5, 15, 198, 270); 


	
	}
		$pdf->lastPage();
    	$datenow  = date('Ymd');          
		$path = "download/Data_".$datenow;			
				
		if(is_dir($path) === false){
			mkdir($path,0755, true);
		}				
				
		$pdf->Output($path."/".$policy_id.'.pdf','F');   
				
		$sql_log = "SELECT SENT_NO FROM TXN_LOG_SOAP";
		$sql_log .= " WHERE POLICY_ID = '" . $policy_id ."'";
		$sql_log .= " ORDER BY SENT_NO DESC";
		$result_log = mysql_query($sql_log);
		if ($rs_log = mysql_fetch_array($result_log)) {
			$sent_no = strval(intval($rs_log["SENT_NO"]) + 1);
		} else {
			$sent_no = '1';
		}

		$res = "Success:Send Email Successfully";
		
		$sql_log = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE) VALUES";
		$sql_log .= " ('".$policy_id."', ". $sent_no .", 'EMAIL', now()";
		$sql_log .= ", '".getenv("REMOTE_ADDR")."', '1', '" . $res . "', '')";
		mysql_unbuffered_query($sql_log);

		$return  = array(
							"STATUS"=>"Success",
							"MESSAGE"=>"Successfully",
							);
								
		$res		= json_encode($return);
 
		// ------ POLICY ORIGINAL Siam PA -------//               
		
		$startdate = PrintDate($rs['POLICY_COMDATE']);
		$enddate = PrintDate($rs['POLICY_EXPDATE']);
		$tmp01 = PrintDate($rs['POLICY_TRANDATE']);

		// ------- /SEND EMAIL TO CUSTOMER ------- //

		$from 		= "fwdgi.web@fwdgi.co.th";
		//$from 		= "siam.web@siamcityinsurance.com";
		//$to 			= "fwdpa.staging.th@edirectinsure.com,tiago.alves@edirectinsure.com,pannita.san@bolttech.io,phumphat.son@fwdgi.com"; //UAT
		//$to 			= $rs['emailAddr']; 
		//$to 			= "fwdpa.staging.th@edirectinsure.com"; //UAT
		$to 			= "fwdpa.th@edirectinsure.com"; //PROD
		$subject 	= "FWDGI-".$policy_id;	
		$msg 		= "<table width='50%' align='center' border='0'>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/BN_1.png' width='947' height='84'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/Line.jpg' width='947' height='5'/ alt='Line'></td>
							</tr>
							<tr>
								<td width='10% colspan='5'></td>
							</tr>
							<tr>
								<td width='5%'></td>
								<td colspan='3'><strong><font color='#e87722' face='Tahoma' size='4'>เรียน คุณ".$rs['tempFirstLifeNm']."    ".$rs['tempLastLifeNm']."</font></strong></td>
								<td width='5%'></td>
							</tr>
							<tr>
								<td width='5%'></td>
								<td colspan='3'><font color='#183028' face='Tahoma' size='3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บมจ. เอฟดับบลิวดีประกันภัย ได้รับข้อมูลการชำระค่าเบี้ยประกันของท่านเรียบร้อยแล้ว เมื่อวันที่ ".$tmp01." เวลา ".substr($rs['POLICY_TRANDATE'],11,5)." น. เพื่อยืนยันความคุ้มครอง บริษัทฯ ได้แนบหน้าตารางกรมธรรม์ อิเล็กทรอนิกส์มาให้ท่านตามอีเมลนี้</font></td>
								<td width='5%'></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><strong><font color='#183028' face='Tahoma' size='5'>เลขกรมธรรม์ของคุณคือ</font></strong></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><strong><font color='#e87722' face='Tahoma' size='6'>".$policy_id."</font></strong></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/Line.jpg' width='947' height='5'/ alt='Line'></td>
							</tr>
							<tr>
								<td height='10' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' width='5%'><img src='https://www.siamcityinsurance.com:9843/schedule/images/iconPaper.png' width='20' height='19'></td>
								<td align='left' colspan='4'><strong><font color='#e87722' face='Tahoma' size='4'>รายละเอียดกรมธรรม์"."</font></strong></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>กรมธรรม์ประเภท : "."</font></strong><font color='#183028' face='Tahoma' size='3'>การประกันภัยอุบัติเหตุส่วนบุคคลสยามซิตี้"."</font></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>วันที่เริ่มคุ้มครอง : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$startdate."</font></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>แผนตวามคุ้มครอง : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$rs['tempFormula']."</font></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>วันที่สิ้นสุดความคุ้มครอง : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$enddate."</font></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>ราคา : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".number_format($total_premium,2)." บาท</font></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>บริษัท : "."</font></strong><font color='#183028' face='Tahoma' size='3'>เอฟดับบลิวดีประกันภัย จำกัด (มหาชน)"."</font></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' width='5%'><img src='https://www.siamcityinsurance.com:9843/schedule/images/iconPerson.png' width='20' height='22'></td>
								<td align='left' colspan='4'><strong><font color='#e87722' face='Tahoma' size='4'>รายละเอียดผู้เอาประกัน"."</font></strong></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='4'><strong><font color='#183028' face='Tahoma' size='3'>ชื่อ : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$rs['tempFirstLifeNm']."    ".$rs['tempLastLifeNm']."</font></td>
							<tr>
							   <td></td>
								<td colspan='4'><strong><font color='#183028' face='Tahoma' size='3'>เบอร์โทรศัพท์ : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$rs['tempLifeMobile']."</font></td>
							</tr>
								<td height='10' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/Line.jpg' width='947' height='5'/ alt='Line'></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><font color='#183028' face='Tahoma' size='3'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลโดยอัตโนมัติโดย บมจ. เอฟดับบลิวดีประกันภัย กรุณาอย่าตอบกลับ"."</font></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td width='5%'></td>
								<td width='30%' align='center'></td>
								<td width='30%' align='center'></td>
								<td width='30%' align='center'></td>
								<td  width='5%'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/BN_2.png' width='947' height='43'></td>
							</tr>
							</table>";
				
			$attach1 =$path."/".$policy_id.".pdf";
			//$attach2 ="#.pdf";
								
			//$arrFiles = array($policy_filename,"$attach1","$attach2");
			$arrFiles = array($policy_filename,"$attach1");
							
			SendMail_MultiAttach($from, $to, $subject, $msg, $arrFiles);
			SendMail_MultiAttach($from,'phumphat.son@fwdgi.com', $subject, $msg, $arrFiles);
			//SendMail_MultiAttach($from,'pannita.san@bolttech.io', $subject, $msg, $arrFiles);
			//SendMail_MultiAttach($from,'tiago.alves@edirectinsure.com', $subject, $msg, $arrFiles);
			
			mysql_free_result($result_log);
			mysql_close($conn);
		
		} else {
						
		$sql_log = "SELECT SENT_NO FROM TXN_LOG_SOAP";
		$sql_log .= " WHERE POLICY_ID = '" . $policy_id ."'";
		$sql_log .= " ORDER BY SENT_NO DESC";
		$result_log = mysql_query($sql_log);
		if ($rs_log = mysql_fetch_array($result_log)) {
			$sent_no = strval(intval($rs_log["SENT_NO"]) + 1);
		} else {
			$sent_no = '1';
		}

		$res = "Error:Data Not Found";
		
		$sql_log = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE) VALUES";
		$sql_log .= " ('".$policy_id."', ". $sent_no .", 'EMAIL', now()";
		$sql_log .= ", '".getenv("REMOTE_ADDR")."', 'Error', '" . $res . "', '')";
		mysql_unbuffered_query($sql);
				
		$return  = array(
							"STATUS"=>"Error",
							"MESSAGE"=>"Data Not Found",
							);
										
						$res		= json_encode($return);
			
		mysql_free_result($result_log);
		mysql_close($conn);
		}
	} else {

		$return  = array(
							"STATUS"=>"Error",
							"MESSAGE"=>"Data Not Found",
							);
						
		$res		= json_encode($return);

	}
} else {

$return  = array(
					"STATUS"=>"Fail",
					"MESSAGE"=>"user & password  incorrect",
					);
				
$res	= json_encode($return);
}
echo $res;
return;

?>